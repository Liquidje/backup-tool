# imports
import sched
import time
import os
import shutil
import argparse
import yaml
import hashlib
import sys
import glob


def backup_folder(src, dst, interval, nr_backups, scheduler=None):
    """Backs up files through a scheduler. Uses the global SRC_/DST_FOLDER vars.
    Scheduler logic is from https://stackoverflow.com/a/474543"""
    # List zip files and remove old ones
    existing_zip_files = glob.glob(f"{dst}/*.zip")
    if len(existing_zip_files) > nr_backups:
        existing_zip_files.sort(
            key=lambda path: int(os.path.splitext(os.path.basename(path))[0])
        )
        zips_to_delete = existing_zip_files[: len(existing_zip_files) - nr_backups]
        for path in zips_to_delete:
            os.remove(path)
        if len(zips_to_delete) > 0:
            print(f"deleted files: {", ".join(zips_to_delete)}")

    # Find next filename
    last_existing_file = None
    zip_path = ""
    if len(existing_zip_files) > 0:
        last_existing_file = existing_zip_files[-1]
        filename, ext = os.path.splitext(os.path.basename(last_existing_file))
        zip_path = f"{dst}{os.path.sep}{int(filename) + 1}.zip"
    else:
        zip_path = f"{dst}{os.path.sep}0.zip"

    # Create zip
    shutil.make_archive(zip_path[:-4], "zip", src)
    if last_existing_file is not None:
        last_existing_backup_hash = hashlib.md5(
            open(last_existing_file, "rb").read()
        ).digest()
        current_backup_hash = hashlib.md5(open(zip_path, "rb").read()).digest()
        if last_existing_backup_hash == current_backup_hash:
            os.remove(zip_path)
            print(f"No change detected")
        else:
            print(f"Backed up to {zip_path}")
    else:
        print(f"Backed up to {zip_path}")

    # Schedule next call if passed
    if scheduler is not None:
        scheduler.enter(
            interval, 1, backup_folder, (src, dst, interval, nr_backups, scheduler)
        )


def parse_settings():
    """Parses settings from command line arguments and settings file, and returns the result"""

    parser = argparse.ArgumentParser(
        prog="Backup tool",
        description="Periodically backs up the set folder to a zip file",
        epilog="Repository: https://gitlab.com/Liquidje/backup-tool",
    )
    parser.add_argument(
        "--src",
        action="store",
        help="The source folder to backup",
    )
    parser.add_argument(
        "--dst",
        action="store",
        help='The destination folder for the backups. Defaults to "<script-folder>/backups"',
    )
    parser.add_argument(
        "--i",
        action="store",
        help="The interval in seconds. Defaults to 300 (5 minutes)",
    )
    parser.add_argument(
        "--nr",
        action="store",
        help="The nr of backups to keep (default: 100)",
    )
    parser.add_argument(
        "--s",
        action="store",
        help="The settings file to use (defaults to './settings.yml'). Settings are overwritten by command line params",
        default="./settings.yml",
    )

    # Init settings from args
    settings = parser.parse_args()

    # Overwrite settings from settings file if present
    if os.path.isfile(settings.s):
        print(f"parsing settings file at {settings.s}")
        yml_settings = yaml.safe_load(open("./settings.yml", "r"))
        for attr, value in yml_settings.items():
            if getattr(settings, attr) is None:
                setattr(settings, attr, value)
    else:
        print(f"no settings file found at {settings.s}")

    # If no src is found in the end, early exit
    if settings.src is None:
        print(
            "No src folder set either through arguments or settings file. This is required. Exiting!"
        )
        sys.exit(2)

    # Parse args and set defaults if not provided
    settings.dst = settings.dst if settings.dst is not None else "./backups"
    settings.i = int(settings.i) if settings.i is not None else int(300)
    settings.nr = int(settings.nr) if settings.nr is not None else int(100)

    return settings


def main():
    """Main function, does preparations and finally schedules the backup"""
    # Change working dir to script dir
    abspath = os.path.abspath(sys.argv[0])
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    # parse arguments and set to globals
    args = parse_settings()

    # Create backups folder if not already created
    if not os.path.isdir(args.dst):
        os.mkdir(args.dst)
        print(f"Created backups folder as it didn't exist yet: {args.dst}")

    # Set up a scheduler for backup_folder() and run it
    my_scheduler = sched.scheduler(time.time, time.sleep)
    my_scheduler.enter(
        args.i, 1, backup_folder, (args.src, args.dst, args.i, args.nr, my_scheduler)
    )
    print(
        f"Scheduler started: \n\
        Source folder: {args.src}\n\
        Destination folder: {args.dst}\n\
        Interval: {args.i} seconds\n\
        Nr of backups kept: {args.nr}"
    )

    # Do an initial run
    backup_folder(args.src, args.dst, args.i, args.nr)

    # Start scheduler
    my_scheduler.run()


if __name__ == "__main__":
    main()
