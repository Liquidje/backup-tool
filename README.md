# Backup tool

A simple backup tool which backs up a file periodically. It needs to keep running to create the backups. It is built as a very simply "start and stop" solution.

By default, backs up files to a "backups" folder in the current dir, uses 300s interval, and keeps 100 copies.

You can configure it through command line parameters (through `--param value`, e.g. `--nr 50`) or through a `settings.yml` file. Command-line variables overwrite settings from the yml file.

A source path (`src`) must be set through either method.

## Parameters / settings

keys in `settings.yml` and command-line variables are the same. E.g. `nr: 100` in `settings.yml` is the same as `--nr 100` as the command-line variables. Command-line variables take precedence over `settings.yml`

`src` (**required**) sets source path
`dst` where to store the zips. Defaults to `./backups`
`i` sets interval in seconds
`nr` sets nr of backups to keep
`-h` or `--help` show the help file
`--s` (command-line only) can be used to set a different path for `settings.yml`

## Building for windows

Use `pyinstaller BackupTool.py --onefile`

## Future feature list

* add aliases for command line params
* support single-use/scheduled run and continuous run
* improve performance by not needing to write zip file to disk to detect change
* improve performance by not querying folder/file content every time